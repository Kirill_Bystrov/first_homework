import random
import os
import time_of_function


#создание промежуточного файла размера 1 МБ из случайных строк входного файла
@time_of_function
def one_MB():
    with open('F:/Downloads/3laws.txt_Ascii.txt', 'r') as input, \
         open('F:/Downloads/oneMB.txt', 'w') as output:
        content = input.readlines()
        N = len(content)
        for i in range(9300):
            output.write(content[random.randint(0, N-1)])
        stats = os.stat('F:/Downloads/oneMB.txt')
        print('-'*17 + ' 1 MB ' + '-'*17)
        print(f'Размер выходного файла, МБ: {round(stats.st_size / 2 ** 20, 3)}')

#создание промежуточного файла произвольного размера из случайных строк входного файла
@time_of_function
def multiply_MB(name: str, multiplier: int):
    with open('F:/Downloads/oneMB.txt', 'r') as input, \
         open(f'F:/Downloads/{name}.txt', 'w') as output:
        content = input.read()
        for i in range(multiplier):
            output.write(content)
        stats = os.stat(f'F:/Downloads/{name}.txt')
        print(f'{"-" * 17} {name} {"-" * 17}')
        print(f'Размер выходного файла, МБ: {round(stats.st_size / 2 ** 20, 3)}')