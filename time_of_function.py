import time


#декоратор, измеряющий время выполнения программы
def time_of_function(function):
    def wrapped(*args):
        start_time = time.perf_counter()
        res = function(*args)
        print(f'Время выполнение программы, сек: {round((time.perf_counter() - start_time), 3)}')
        return res
    return wrapped